function map = anteprocTimeShift(map)
% Each anteproc output file contains several time segments of data
% This script shifts these segments to the right, for one of two detectors

nsegs = size(map.P);
nsegs = nsegs(2);

rbartilde(:,2:nsegs) = map.rbartilde(:,1:nsegs-1);
rbartilde(:,1) = map.rbartilde(:,nsegs);

naiP(:,2:nsegs) = map.naiP(:,1:nsegs-1);
naiP(:,1) = map.naiP(:,nsegs);

P(:,2:nsegs) = map.P(:,1:nsegs-1);
P(:,1) = map.P(:,nsegs);

map.rbartilde = rbartilde;
map.naiP = naiP;
map.P = P;

end
