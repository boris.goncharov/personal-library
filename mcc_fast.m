function mcc_fast(fname)
% function mcc_fast(fname)
% example usage: mcc_fast('preproc');

% define required paths for stamp2: 
% each subdirectory should be included separately
stamp2 = {'/home/boris.goncharov/shortcuts/stamp2/src/', ...
'/home/boris.goncharov/shortcuts/stamp2/src/algorithms/stochtrack', ...
'/home/boris.goncharov/shortcuts/stamp2/src/img', ...
'/home/boris.goncharov/shortcuts/stamp2/src/gpu_tools', ...
'/home/boris.goncharov/shortcuts/stamp2/src/inj', ...
'/home/boris.goncharov/shortcuts/stamp2/src/tools', ...
'/home/boris.goncharov/shortcuts/stamp2/src/misc', ...
'/home/boris.goncharov/shortcuts/stamp2/src/misc/utilities', ...
'/home/boris.goncharov/shortcuts/stamp2/input/', ...
'/home/boris.goncharov/shortcuts/stamp2/src/crosscorr'
};

% start off by requiring stamp code
comp_cmd = 'mcc -v -N';
for ii=1:numel(stamp2)
  comp_cmd = [comp_cmd ' -I ' stamp2{ii}];
end

% path to signals toolbox
sigs = '/ldcg/matlab_r2013a/toolbox/signal/signal';
stats = '/ldcg/matlab_r2013a/toolbox/stats/stats';
% add parallel without java to run gather()
% this is needed for stochtrack on a single core
parallel = '/ldcg/matlab_r2013a/toolbox/distcomp/parallel/';
% add this to run stochtrack on a single gpu
gpu1 = '/ldcg/matlab_r2013a/toolbox/distcomp/gpu/';
gpu2 = '/ldcg/matlab_r2013a/toolbox/distcomp/array';
% in order to use padarray.m (Thanks, Michael)
images = '/ldcg/matlab_r2013a/toolbox/images/images/';
% distributed computing (THIS DOES NOT WORK...without java)
%dist = '/ldcg/matlab_r2013a/toolbox/distcomp/';

% java
%java = '/ldcg/matlab_r2013a/toolbox/javabuilder/javabuilder';

% -I flags
includes = { sigs stats parallel images gpu1 gpu2 };

% -R flags
options = { '-nodisplay','-nojvm','-singleCompThread' };

% compilation command includes
for ii=1:numel(includes)
  comp_cmd = [comp_cmd ' -I ' includes{ii}];
end

% ...options
for ii=1:numel(options)
  comp_cmd = [comp_cmd ' -R ' options{ii}];
end

% ...add -m flag
comp_cmd = [comp_cmd ' -m ' fname];

% compile
fprintf('%s\n',comp_cmd);
eval(comp_cmd);

% remove annoying files
system(['rm mccExcludedFiles.log readme.txt run_' fname '.sh']);

return;
