function [y, index1, index2, frac1, frac2]=coarseGrain(x, flowy, deltaFy, Ny)
%
%  coarseGrain --- coarse grain a frequency-series 
%
%  coarseGrain(x, flowy, deltaFy, Ny) returns a frequency-series structure 
%  coarse-grained to the frequency values f = flowy + deltaFy*[0:Ny-1].  
%
%  coarseGrain also returns the indices of the lowest and highest frequency 
%  bins of x that overlap with y (0 <= index1 <= length(x); 
%  1 <= index2 <= length(x)+1) and the fractional contributions from these 
%  frequency bins.
%
%  Routine written by Joseph D. Romano.
%  Contact Joseph.Romano@astro.cf.ac.uk
%  Please see this Nov 13, 2014 ilog entry for additional information:
%  http://tinyurl.com/l3z25kl
%
%  $Id: coarseGrain.m,v 1.4 2005-02-24 14:36:27 jromano Exp $
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% BORIS: Arithmetically averaging old bins for every new bin
% For NaN's number of bins in the denominator will be substracted
% And NaNs in the nominator will be turned to zeros for correct summation
% Assuming, flow is the lower boundary of the first bin

% extract metadata
Nx = length(x.data);
flowx = x.flow;
deltaFx = x.deltaF;

% error checking %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% check that frequency spacings and number of elements are > 0
if ( (deltaFx <= 0) | (deltaFy <= 0) | (Ny <= 0) )
  error('bad input arguments');
end

% check that freq resolution of x is finer than desired freq resolution
if ( deltaFy < deltaFx )
  error('deltaF coarse-grain < deltaF fine-grain');
end

% BORIS: loosened limitations on low and high frequencies
% check desired start frequency for coarse-grained series
if flowy < flowx %( (flowy - 0.5*deltaFy) < (flowx - 0.5*deltaFx) )
  error('desired coarse-grained start frequency is too low');
end

% check desired stop frequency for coarse-grained series
fhighx = flowx + (Nx-1)*deltaFx;
fhighy = flowy + (Ny-1)*deltaFy;
if fhighy > fhighx %( (fhighy + 0.5*deltaFy) > (fhighx + 0.5*deltaFx) )
  error('desired coarse-grained stop frequency is too high');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% indices for coarse-grained series
i = transpose([0:1:Ny-1]);

% calculate the low and high indices of fine-grained series 
jlow  = 1 + floor( (flowy + (i-0.5)*deltaFy - flowx - 0.5*deltaFx)/deltaFx ); 
jhigh = 1 + floor( (flowy + (i+0.5)*deltaFy - flowx - 0.5*deltaFx)/deltaFx ); 
index1 = jlow(1);
index2 = jhigh(end);

% calculate fractional contributions
fraclow  = (flowx + (jlow+0.5)*deltaFx - flowy - (i-0.5)*deltaFy)/deltaFx;
frachigh = (flowy + (i+0.5)*deltaFy - flowx - (jhigh-0.5)*deltaFx)/deltaFx;
frac1 = fraclow(1);
frac2 = frachigh(end);

% BORIS: some changes to jlow and jhigh, define new frac
jlow = jlow + (deltaFy/deltaFx)/2 + 1;
jhigh = jlow + deltaFy/deltaFx - 1;
frac = transpose(linspace(deltaFx/deltaFy,deltaFx/deltaFy,Ny));

% BORIS: locate NaNs and make them zeros
nanLoc = find(isnan(x.data));
x.data(nanLoc) = 0;

% BORIS: averaging coefficients, accounting for NaNs
nanBin = [];
for ii=1:Ny
  nanBin = [nanBin;length(find((nanLoc>=jlow(ii))&(nanLoc<=jhigh(ii))))];
end
nanBin = repelem(nanBin,deltaFy/deltaFx);
dFx(1:length(nanBin),1)=deltaFx;
dFy(1:length(nanBin),1)=deltaFy;
frac = 1./(((1./dFx)./(1./dFy))-nanBin);
frac(isnan(frac))=0;

% BORIS: Averaging (coarse-graining) data
x.data(jlow(1):jhigh(end)) = x.data(jlow(1):jhigh(end)).*frac;

% calculate coarse-grained values %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sum of middle terms
% NOTE:  since this operation takes more time than anything else, i made 
% it into separate routine called sumTerms() which is then converted into
% a MEX file
%jtemp = jlow+2; % WHAT IS THIS? FIRST IS ADDED LATER, OK, BUT 2ND IGNORED???

%[midsum_real,midsum_complex] = sumTerms(...
%   real(x.data),imag(x.data), jlow, jhigh, Ny);
%midsum = complex(midsum_real,midsum_complex);

y.data = [];
for ii=1:Ny
  y.data = [y.data;sum(x.data(jlow(ii):jhigh(ii)))];
end

%midsum = sumTerms(x.data, jtemp, jhigh);

% calculate all but final value of y
%ya = (deltaFx/deltaFy)*(x.data(jlow (1:Ny-1)+1).*fraclow (1:Ny-1) + ...
%                        x.data(jhigh(1:Ny-1)+1).*frachigh(1:Ny-1) + ...
%                        midsum(1:Ny-1) );

% calculate final value of y
%if (jhigh(Ny) > Nx-1)
%  % special case when jhigh exceeds maximum allowed value
%  yb = (deltaFx/deltaFy)*(x.data(jlow(Ny)+1)*fraclow(Ny) + midsum(Ny));

%else
%  yb = (deltaFx/deltaFy)*(x.data(jlow (Ny)+1)*fraclow (Ny) + ...
%                          x.data(jhigh(Ny)+1)*frachigh(Ny) + ...
%                          midsum(Ny) );
%end

% fill structure for coarsed-grained frequency series
%%y.data = midsum[ya; yb];
y.flow = flowy;
y.deltaF = deltaFy;
try
  y.symmetry = x.symmetry;
catch
  y.symmetry = 0;
end

return

